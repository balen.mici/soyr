#!/bin/bash

# Verificar que se proporcionaron los argumentos correctos
if [ $# -ne 2 ]; then
    echo "Uso: $0 <directorio> <N>"
    exit 1
fi

directorio="$1"
N="$2"

# Verificar que N esté en el rango válido
if [ "$N" -lt 1 ] || [ "$N" -gt 100 ]; then
    echo "N debe estar comprendido entre 1 y 100."
    exit 1
fi

# Listar los archivos en el directorio y mostrar las primeras N líneas de cada uno
for archivo in "$directorio"/*; do
    if [ -f "$archivo" ]; then
        echo "Primeras $N líneas de $archivo:"
        head -n "$N" "$archivo"
        echo "---------------------------------"
    fi
done
